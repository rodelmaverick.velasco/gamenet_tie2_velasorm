using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakeDamage : MonoBehaviourPunCallbacks
{

    [SerializeField] Image healthbar;
    private float startHealth = 100;
    public float health;
    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        healthbar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void TakingDamage(int damage)
    {
        health -= damage;
        Debug.Log(health);

        healthbar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die();
        }
    }
    private void Die()
    {
         Debug.Log("dead");
        if (photonView.IsMine)
        {
            GameManager.instance.LeaveRoom();
        }
        
    }
}
